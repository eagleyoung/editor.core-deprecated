﻿using Lib.Core;
using System.IO;
using System.Reflection;

namespace Editor.Core.Data
{
    /// <summary>
    /// Менеджер экспортирования данных
    /// </summary>
    public static class ExportHandler
    {
        /// <summary>
        /// Папка с данными
        /// </summary>
        static DirectoryInfo dataFolder;

        static ExportHandler()
        {
            var assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                var curDir = new DirectoryInfo(Path.GetDirectoryName(assembly.Location));
                var dir = curDir.Parent;
                
                foreach(var childDir in dir.GetDirectories("*", SearchOption.AllDirectories))
                {
                    if(childDir.Name == "Resources")
                    {
                        dataFolder = new DirectoryInfo(Path.Combine(childDir.FullName, "Data"));
                        if (!dataFolder.Exists)
                        {
                            dataFolder.Create();
                        }
                        return;
                    }
                }
            }

            if(dataFolder == null)
            {
                Output.Error("Отсутствует папка с данными");
            }
        }

        /// <summary>
        /// Сохранить данные определенного ключа
        /// </summary>
        public static void Save(string name, string saveString)
        {
            if (dataFolder == null) return;

            var path = Path.Combine(dataFolder.FullName, $"{name}.txt");

            File.WriteAllBytes(path, saveString.Encrypt());
        }
    }
}
