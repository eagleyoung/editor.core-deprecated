﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Editor.Core.Data
{
    /// <summary>
    /// Менеджер улучшений
    /// </summary>
    public class UpgradeManager : BaseDataManager<UpgradeData>
    {
        static Lazy<UpgradeManager> instance = new Lazy<UpgradeManager>(()=> BaseDataManager.Create<UpgradeManager,UpgradeData>("Upgrade"), true);

        public override string Name => "Улучшения";

        public static UpgradeManager GetInstance()
        {
            return instance.Value;
        }

        public override void Save()
        {
            BaseDataManager.Save(this, "Upgrade");
        }

        public override void Export()
        {
            var state = JObject.FromObject(this);

            ExportHandler.Save("Upgrade", state.ToString(Formatting.Indented));
        }
    }
}
