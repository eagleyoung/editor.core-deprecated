﻿using CsvHelper;
using Lib.WPF;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Text;

namespace Editor.Core.Data
{
    /// <summary>
    /// Менеджер переводов
    /// </summary>
    public class LocManager : BaseDataManager<LocData>
    {
        static Lazy<LocManager> instance = new Lazy<LocManager>(() => {
            var inst = BaseDataManager.Create<LocManager, LocData>("Loc");
            var filter = new FilterItem(FilterLoc);
            inst.Items.AttachFilter(filter);
            return inst;
        }, true);

        static void FilterLoc(object item, FilterEventArgs e)
        {
            var loc = item as LocData;
            if (loc.IsExternal) e.Enabled = false;
        }

        public override string Name => "Локализация";

        public static LocManager GetInstance()
        {
            return instance.Value;
        }

        public override void Save()
        {
            BaseDataManager.Save(this, "Loc");
        }

        /// <summary>
        /// Получить объект по ключу. При отсутствии объекта создает новый
        /// </summary>
        public static LocData Get(string key)
        {
            var inst = GetInstance();

            foreach(var item in inst.Items) {
                if (item.Key == key) {
                    item.IsExternal = true;
                    return item;
                }
            }

            var newItem = new LocData() { Key = key, IsExternal = true };
            inst.ProceedAdd(newItem);
            inst.Items.Add(newItem);

            return newItem;
        }

        public override void Export()
        {
            var state = JObject.FromObject(this);

            state.RemoveRecursive("IsExternal");
            
            ExportHandler.Save("Loc", state.ToString(Formatting.Indented));
        }

        public LocManager()
        {
            ExportCSVCommand = new Command(ExportCSV);
            ImportCSVCommand = new Command(ImportCSV);
        }

        [JsonIgnore]
        public Command ExportCSVCommand { get; }
        /// <summary>
        /// Экспортировать файл формата CSV с переводом
        /// </summary>
        void ExportCSV()
        {
            var path = Helper.GetFileSavePath("Localization", ".csv", "CSV files (.csv)|*.csv");

            if(path != null)
            {
                using (var writer = new StreamWriter(path, false, new UTF8Encoding(true)))
                {
                    var csv = new CsvWriter(writer);
                    csv.Configuration.RegisterClassMap(new LocDataMap());
                    csv.WriteRecords(Items);                    
                }
                Utilities.ShowPopup("Перевод экспортирован");
            }
        }

        [JsonIgnore]
        public Command ImportCSVCommand { get; }
        /// <summary>
        /// Импортировать файл формата CSV с переводом
        /// </summary>
        void ImportCSV()
        {
            var path = Helper.GetFileOpenPath(".csv", "CSV files (.csv)|*.csv");

            if(path != null)
            {
                using (var reader = new StreamReader(path))
                {
                    var csv = new CsvReader(reader);
                    csv.Configuration.RegisterClassMap(new LocDataMap());
                    var items = csv.GetRecords<LocData>();

                    foreach(var item in items)
                    {
                        var loc = Get(item.Key);
                        loc.En = item.En;
                        loc.Ru = item.Ru;
                    }
                }
                Utilities.ShowPopup("Перевод импортирован");
            }
        }
    }
}
