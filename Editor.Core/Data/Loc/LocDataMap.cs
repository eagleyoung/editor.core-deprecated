﻿using CsvHelper.Configuration;

namespace Editor.Core.Data
{
    /// <summary>
    /// Данные для форматирования CSV файла
    /// </summary>
    public sealed class LocDataMap : ClassMap<LocData>
    {
        public LocDataMap()
        {
            AutoMap();
            Map(m => m.IsExternal).Ignore();
            Map(m => m.Key).Index(0);
            Map(m => m.En).Index(1);
            Map(m => m.Ru).Index(2);
        }
    }
}
