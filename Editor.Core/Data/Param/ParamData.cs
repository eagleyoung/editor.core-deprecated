﻿using DataInterface.Core;
using Lib.WPF;

namespace Editor.Core.Data
{
    /// <summary>
    /// Сохраняемые данные
    /// </summary>
    public class ParamData : BaseData, IParamData
    {        
        string inner;
        /// <summary>
        /// Внутреннее представление значения
        /// </summary>
        public string Inner
        {
            get
            {
                return inner;
            }
            set
            {
                if(inner != value)
                {
                    inner = value;
                    OnPropertyChanged();
                }
            }
        }

        bool mustDrop;
        /// <summary>
        /// Параметр должен быть сброшен при сбросе всех данных?
        /// </summary>
        public bool MustDrop {
            get
            {
                return mustDrop;
            }
            set
            {
                if(mustDrop != value)
                {
                    mustDrop = value;
                    OnPropertyChanged();
                }
            }
        }

        ParamTypes paramType;
        /// <summary>
        /// Текущий тип переменной
        /// </summary>
        public ParamTypes ParamType {
            get
            {
                return paramType;
            }
            set
            {
                if(paramType != value)
                {
                    paramType = value;
                    OnPropertyChanged();
                }
            }
        }

        string description;
        /// <summary>
        /// Описание параметра
        /// </summary>
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                if(description != value)
                {
                    description = value;
                    OnPropertyChanged();
                }
            }
        }

        protected override SearchDataPack GenerateSearch()
        {
            var pack = new SearchDataPack();
            pack.Add(Key);
            pack.Add(ParamType.ToString());
            pack.Add(Description);
            return pack;
        }
    }
}
