﻿using DataInterface.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Editor.Core.Data
{
    /// <summary>
    /// Менеджер сохраняемых данных
    /// </summary>
    public class ParamManager : BaseDataManager<ParamData>
    {
        static Lazy<ParamManager> instance = new Lazy<ParamManager>(()=> BaseDataManager.Create<ParamManager, ParamData>("Param"), true);

        public override string Name => "Параметры";

        public static ParamManager GetInstance()
        {
            return instance.Value;
        }

        [JsonIgnore]
        /// <summary>
        /// Доступные типы параметров
        /// </summary>
        public IEnumerable<ParamTypes> ParamTypes
        {
            get
            {
                return Enum.GetValues(typeof(ParamTypes)).Cast<ParamTypes>();
            }
        }

        public override void Save()
        {
            BaseDataManager.Save(this, "Param");
        }

        public override void Export()
        {
            var state = JObject.FromObject(this);

            state.RemoveRecursive("Description");

            ExportHandler.Save("Param", state.ToString(Formatting.Indented));
        }
    }
}
