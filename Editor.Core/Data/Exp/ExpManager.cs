﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;

namespace Editor.Core.Data
{
    /// <summary>
    /// Менеджер опыта
    /// </summary>
    public class ExpManager : BaseDataManager<ExpData>
    {
        static Lazy<ExpManager> instance = new Lazy<ExpManager>(()=> BaseDataManager.Create<ExpManager, ExpData>("Exp"), true);
        public static ExpManager GetInstance()
        {
            return instance.Value;
        }

        public override void Save()
        {
            BaseDataManager.Save(this, "Exp");
        }

        public override void Export()
        {
            var state = JObject.FromObject(this);
            ExportHandler.Save("Exp", state.ToString(Formatting.Indented));
        }

        public override string Name => "Опыт";

    }
}
