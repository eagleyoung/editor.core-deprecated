﻿using Lib.Core;
using System;
using System.IO;
using System.Reflection;

namespace Editor.Core.Data
{
    /// <summary>
    /// Объект для управления бэкапами данных
    /// </summary>
    public static class BackupHandler
    {
        /// <summary>
        /// Папка с данными бэкапа
        /// </summary>
        static DirectoryInfo backupFolder;

        /// <summary>
        /// Количество бэкапов файла
        /// </summary>
        static int backupCount;

        static BackupHandler()
        {
            backupCount = Config.ByKey("Core.DataBackupCount")?.Int ?? 0;

            var assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                backupFolder = new DirectoryInfo($"{Path.GetDirectoryName(assembly.Location)}/Backup");
                if (!backupFolder.Exists)
                {
                    backupFolder.Create();
                }
            }
        }

        /// <summary>
        /// Зарегистрировать файл для бэкапа
        /// </summary>
        public static void Register(string path)
        {
            var info = new FileInfo(path);

            var dir = new DirectoryInfo(Path.Combine(backupFolder.FullName, info.Name));
            if (!dir.Exists)
            {
                dir.Create();
            }

            File.Copy(path, Path.Combine(dir.FullName, DateTime.Now.Writable()), true);

            var files = dir.GetFiles("*", SearchOption.TopDirectoryOnly);
            if(files.Length > backupCount)
            {
                FileInfo top = null;
                foreach(var file in files)
                {
                    if(top == null || top.CreationTime > file.CreationTime)
                    {
                        top = file;
                    }
                }

                if(top != null)
                {
                    top.Delete();
                }
            }
        }
    }
}
