﻿using Newtonsoft.Json;
using System;
using System.ComponentModel;

namespace Editor.Core.Data
{
    /// <summary>
    /// Объект с данными о переводе
    /// </summary>
    public abstract class LocalizedDataObject : BaseData
    {
        /// <summary>
        /// Ключ для загрузки перевода объекта
        /// </summary>
        protected abstract string LocKey { get; }

        Lazy<LocData> locs;
        [JsonIgnore]
        /// <summary>
        /// Переводы объекта
        /// </summary>
        public virtual LocData Locs
        {
            get
            {
                return locs.Value;
            }
        }

        public override string Key
        {
            get => base.Key;
            set
            {
                base.Key = value;
                Locs.Key = LocKey;
            }
        }

        public LocalizedDataObject()
        {
            locs = new Lazy<LocData>(() => {
                var data = LocManager.Get(LocKey);
                data.PropertyChanged += LocChanged;
                return data;
            });
        }

        protected void LocChanged(object sender, PropertyChangedEventArgs e)
        {
            search.Clear();
        }
    }
}
