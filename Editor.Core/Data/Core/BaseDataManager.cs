﻿using Lib.Core;
using Lib.WPF;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Editor.Core.Data
{
    /// <summary>
    /// Базовый менеджер данных
    /// </summary>
    public abstract class BaseDataManager<T> : NotifyObject, IDataManager
        where T : BaseData, new()
    {        
        [JsonIgnore]
        /// <summary>
        /// Наименование менеджера
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// Сохранить данные
        /// </summary>
        public abstract void Save();

        /// <summary>
        /// Экспортировать данные
        /// </summary>
        public abstract void Export();

        ~BaseDataManager()
        {
            Save();
        }

        /// <summary>
        /// Доступные элементы
        /// </summary>
        public SearchableCollection<T> Items { get; set; } = SearchableCollection.Create<T>(new BaseDataComparer());

        /// <summary>
        /// Осуществить поиск по набору
        /// </summary>
        public void Search(string search)
        {
            Items.Search(search);
        }

        protected BaseDataManager()
        {
            AddCommand = new Command(Add);
            RemoveCommand = new Command(Remove);
        }

        [JsonIgnore]
        public Command AddCommand { get; }
        /// <summary>
        /// Добавить новый элемент
        /// </summary>
        void Add()
        {
            var item = new T();
            ProceedAdd(item);
            Items.Add(item);
        }

        /// <summary>
        /// Инициализация объекта
        /// </summary>
        protected virtual void ProceedAdd(T item) { }

        [JsonIgnore]
        public Command RemoveCommand { get; }
        /// <summary>
        /// Удалить элемент
        /// </summary>
        void Remove(object obj)
        {
            if(Utilities.GetBool("Удалить элемент?"))
            {
                var item = obj as T;
                Items.Remove(item);
                ProceedRemove(item);
            }            
        }

        /// <summary>
        /// Удаление объекта
        /// </summary>
        protected virtual void ProceedRemove(T item) { }
    }

    public static class BaseDataManager
    {
        /// <summary>
        /// Сохранить все менеджеры
        /// </summary>
        public static void SaveAll()
        {
            foreach(var manager in managers)
            {
                manager.Save();
            }
        }

        /// <summary>
        /// Экспортировать все менеджеры
        /// </summary>
        public static void ExportAll()
        {
            foreach(var manager in managers)
            {
                manager.Export();
            }
        }

        /// <summary>
        /// Список всех менеджеров
        /// </summary>
        static List<IDataManager> managers = new List<IDataManager>();

        /// <summary>
        /// Загрузить менеджер данных
        /// </summary>
        public static TManager Create<TManager, TItem>(string dataPath)
            where TManager : BaseDataManager<TItem>, new()
            where TItem : BaseData, new()
        {
            var initialState = SaveHandler.Load(dataPath);
            TManager output = null;
            if (initialState != null)
            {
                output = JsonConvert.DeserializeObject<TManager>(initialState);
            }
            else
            {
                output = new TManager();
            }
            managers.Add(output);
            return output;
        }

        /// <summary>
        /// Сохранить менеджер данных
        /// </summary>
        public static void Save(object obj, string dataPath)
        {
            var state = JObject.FromObject(obj);
            SaveHandler.Save(dataPath, state.ToString(Formatting.Indented));
        }
    }
}
