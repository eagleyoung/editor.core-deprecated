﻿using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Editor.Core.Data
{
    public class BaseDataComparer : Comparer<BaseData>
    {
        [DllImport("shlwapi.dll", CharSet = CharSet.Unicode)]
        private static extern int StrCmpLogicalW(string psz1, string psz2);

        public override int Compare(BaseData x, BaseData y)
        {
            if (x.Key == null) return -1;
            if (y.Key == null) return 1;

            return StrCmpLogicalW(x.Key, y.Key);
        }
    }
}
