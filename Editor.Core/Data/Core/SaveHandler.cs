﻿using System.IO;
using System.Reflection;

namespace Editor.Core.Data
{
    /// <summary>
    /// Менеджер сохраняемых данных
    /// </summary>
    public static class SaveHandler
    {
        /// <summary>
        /// Папка с актуальными данными
        /// </summary>
        static DirectoryInfo saveFolder;
        
        static SaveHandler()
        {
            var assembly = Assembly.GetEntryAssembly();
            if (assembly != null)
            {
                saveFolder = new DirectoryInfo($"{Path.GetDirectoryName(assembly.Location)}/Data");
                if (!saveFolder.Exists)
                {
                    saveFolder.Create();
                }
            }
        }

        /// <summary>
        /// Загрузить данные по определенному ключу
        /// </summary>
        public static string Load(string name)
        {
            if (saveFolder == null) return null;

            var path = Path.Combine(saveFolder.FullName, $"{name}.txt");
            if (File.Exists(path))
            {
                return File.ReadAllText(path);
            }
            return null;
        }

        /// <summary>
        /// Сохранить данные определенного ключа
        /// </summary>
        public static void Save(string name, string saveString)
        {
            if (saveFolder == null) return;

            var path = Path.Combine(saveFolder.FullName, $"{name}.txt");

            File.WriteAllText(path, saveString);

            BackupHandler.Register(path);
        }
    }
}
