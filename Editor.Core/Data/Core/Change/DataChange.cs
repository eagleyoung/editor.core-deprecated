﻿using DataInterface.Core;
using Lib.WPF;

namespace Editor.Core.Data
{
    /// <summary>
    /// Параметры изменения
    /// </summary>
    public class DataChange : NotifyObject, IDataChange
    {
        string paramKey;
        /// <summary>
        /// Ключ параметра для изменения
        /// </summary>
        public string ParamKey {
            get
            {
                return paramKey;
            }
            set
            {
                if(paramKey != value) {
                    paramKey = value;
                    OnPropertyChanged();
                }
            }
        }

        string val;
        /// <summary>
        /// Изменение
        /// </summary>
        public string Value {
            get
            {
                return val;
            }
            set
            {
                if (val != value) {
                    val = value;
                    OnPropertyChanged();
                }
            }
        }

        string multiplierKey;
        /// <summary>
        /// Ключ параметра - множителя
        /// </summary>
        public string MultiplierKey
        {
            get
            {
                return multiplierKey;
            }
            set
            {
                if (multiplierKey != value)
                {
                    multiplierKey = value;
                    OnPropertyChanged();
                }
            }
        }
    }
}
