﻿using GameEditor.Model;
using System.Collections.Generic;
using WpfCore;

namespace GameEditor.Formula
{
    public static class FormulaSolver
    {
        static Dictionary<string, FormulaItem> byId = new Dictionary<string, FormulaItem>();
        public static object ById(string key)
        {
            if (byId.TryGetValue(key, out FormulaItem output))
            {
                return output.Value;
            }
            return null;
        }

        static Queue<FormulaItem> solveQueue = new Queue<FormulaItem>();

        static void SolveAll()
        {
            Queue<FormulaItem> unsolved = new Queue<FormulaItem>();
            while(solveQueue.Count > 0)
            {
                var item = solveQueue.Dequeue();
                item.Solve();
                if (item.ErrorString != null)
                {
                    unsolved.Enqueue(item);
                }
            }
            solveQueue = unsolved;
        }

        static void CheckDependencies(FormulaItem item)
        {
            foreach(var formula in byId.Values)
            {
                if (formula == item) continue;
                if (formula.IsDependentFrom(item.FormulaId))
                {
                    if (!solveQueue.Contains(formula))
                    {
                        solveQueue.Enqueue(formula);
                        CheckDependencies(formula);
                    }
                }
            }
        }

        public static void Solve(FormulaItem item)
        {
            if (!solveQueue.Contains(item))
            {
                solveQueue.Enqueue(item);
            }
            CheckDependencies(item);
            SolveAll();
        }

        public static void ChangeId(FormulaItem item, string _newId)
        {
            if(item.FormulaId != null)
            {
                if (byId.ContainsKey(item.FormulaId))
                {
                    byId.Remove(item.FormulaId);
                }                
            }

            if (!byId.ContainsKey(_newId))
            {
                byId.Add(_newId, item);
            }
            else
            {
                Output.Error("Дублирование идентификатора формулы {0}", _newId);
            }
        }

        public static void Register(FormulaItem item)
        {
            if (item.FormulaId == null) return;
            if (!byId.ContainsKey(item.FormulaId))
            {
                byId.Add(item.FormulaId, item);
            }
            else
            {
                Output.Error("Дублирование идентификатора формулы {0}", item.FormulaId);
            }
        }

        public static void Unregister(FormulaItem item)
        {
            if (item.FormulaId == null) return;
            if (byId.ContainsKey(item.FormulaId))
            {
                byId.Remove(item.FormulaId);
            }
        }

        public static void Clear()
        {
            byId.Clear();
        }
    }
}
