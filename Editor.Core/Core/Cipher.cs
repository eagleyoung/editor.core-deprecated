﻿using Lib.Shared.Encryption;

namespace Editor.Core
{
    /// <summary>
    /// Класс для осуществления шифровки строковых данных
    /// </summary>
    public static class Cipher
    {
        /// <summary>
        /// Объект для производства шифровки
        /// </summary>
        static Encryptor encryptor;
        
        static Cipher()
        {
            encryptor = new Encryptor(new CipherKey());
        }

        /// <summary>
        /// Зашифровать строку
        /// </summary>
        public static byte[] Encrypt(this string plainText)
        {
            return encryptor.Encrypt(plainText);
        }

        /// <summary>
        /// Расшифровать строку
        /// </summary>
        public static string Decrypt(this byte[] cipherText)
        {
            return encryptor.Decrypt(cipherText);
        }
    }
}
