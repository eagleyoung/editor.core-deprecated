﻿using Editor.Core.Data;
using Lib.WPF;

namespace Editor.Core
{
    /// <summary>
    /// Базовая модель представления окна редактора
    /// </summary>
    public class EditorViewModel : BaseViewModel
    {
        /// <summary>
        /// Менеджер данных
        /// </summary>
        public ParamManager Param { get; } = ParamManager.GetInstance();

        /// <summary>
        /// Менеджер перевода
        /// </summary>
        public LocManager Loc { get; } = LocManager.GetInstance();
    }
}
