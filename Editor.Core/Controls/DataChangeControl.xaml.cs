﻿using Editor.Core.Data;
using Lib.Core;
using Lib.WPF;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Editor.Core.Controls
{
    /// <summary>
    /// Логика взаимодействия для DataChangeControl.xaml
    /// </summary>
    public partial class DataChangeControl : UserControl
    {
        public DataChangeControl()
        {
            InitializeComponent();
        }

        private void AddClick(object sender, RoutedEventArgs e)
        {
            var newChange = new DataChange();
            var collection = (DataContext as ICollection<DataChange>);

            if (collection == null)
            {
                Output.ErrorAdditional("Ошибка при выполнении", "Контекст выполнения не содержит коллекцию элементов");
                return;
            }

            collection.Add(newChange);
        }

        private void DeleteChange(object sender, RoutedEventArgs e)
        {
            var change = (sender as FrameworkElement).DataContext as DataChange;
            var collection = (DataContext as ICollection<DataChange>);

            if (collection == null)
            {
                Output.ErrorAdditional("Ошибка при выполнении", "Контекст выполнения не содержит коллекцию элементов");
                return;
            }

            if(Utilities.GetBool("Удалить изменение?"))
            {
                collection.Remove(change);
            }            
        }
    }
}
