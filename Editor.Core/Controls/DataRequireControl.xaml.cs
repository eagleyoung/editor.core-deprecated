﻿using Editor.Core.Data;
using Lib.Core;
using Lib.WPF;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace Editor.Core.Controls
{
    /// <summary>
    /// Логика взаимодействия для DataRequireControl.xaml
    /// </summary>
    public partial class DataRequireControl : UserControl
    {
        public DataRequireControl()
        {
            InitializeComponent();
        }

        private void AddClick(object sender, RoutedEventArgs e)
        {
            var newRequire = new DataRequire();
            var collection = (DataContext as ICollection<DataRequire>);

            if (collection == null)
            {
                Output.ErrorAdditional("Ошибка при выполнении", "Контекст выполнения не содержит коллекцию элементов");
                return;
            }

            collection.Add(newRequire);
        }

        private void DeleteRequire(object sender, RoutedEventArgs e)
        {
            var require = (sender as FrameworkElement).DataContext as DataRequire;
            var collection = (DataContext as ICollection<DataRequire>);

            if (collection == null)
            {
                Output.ErrorAdditional("Ошибка при выполнении", "Контекст выполнения не содержит коллекцию элементов");
                return;
            }

            if(Utilities.GetBool("Удалить требование?"))
            {
                collection.Remove(require);
            }            
        }
    }
}
